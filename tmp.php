<?php
# cet exemple est l'une des réponses possibles pour l'exercice 3 de l'exam du 4 novembre 2016 ( matin )
# il vise à montrer l'usage d'un nombre variable d'arguments pour une fonction sans pour autant passer par une surcharge de fonction
# exemple de surcharge :
# function manger($var1 = null, $var2 = null)
# ce cas n'est pas forcément une bonne idée ( dans ce cas du moins ) : il ne vous est pas possible de savoir si la fonction est
#     appellée avec 0 arguments, un argument ( null ) ou  deux arguments ( null, null )
# php possède des fonctionnalités qui permettent d'éviter ce problème :
# http://www.php.net/manual/fr/functions.arguments.php#functions.variable-arg-list
# l'exemple ci dessous est l'une des implémentations possibles

# fonction déclarée sans arguments
function manger() {
	# déclaration d'un tableau vide
	$vars = [];
	# avec cette boucle for, on vient récupérer tous les arguments passés à la fonction pour les mettre dans le tableau
	for ($i = 0; $i < func_num_args(); $i++) {
		# ici, l'argument numéro $i est placé à la fin du tableau
		$vars[] = func_get_arg($i);
	}
	# si le tableau est vide, la fonction n'a pas reçu d'arguments du tout
	if (sizeof($vars) == 0) {
		return false;
	}
	# si le tableau contient 1 élément, au moins une ariable a été passée à la fonction ( et ainsi de suite )
	if (sizeof($vars) == 1) {
		# ici, on regarde si le premier argument est un string et que le string est vide ou si le premier argument est null
		if ($vars[0] === "" || $vars[0] === null) {
			return false;
		}
		if ($vars[0] === "banane") {
			return "1 banane";
		}
	} elseif (sizeof($vars) == 2) {	
		if ($vars[0] === "banane" && $vars[1] === "jaune") {
			return "1 banane jaune";
		}
	} elseif (sizeof($vars) == 3) {
		if ($vars[0] === "banane" && $vars[1] === "bleue" && $vars[2] === null) {
			return false;
		}
		if ($vars[0] === "banane" && $vars[1] === "mauve" && $vars[2] === 0) {
			return "0 banane mauve";
		}
		if ($vars[0] === "banane" && $vars[1] === "jaune" && $vars[2] === 4) {
			return "4 bananes jaunes";
		}
		if ($vars[0] === "abricot" && $vars[1] === "orange" && $vars[2] === 1) {
			return "1 abricot orange";
		}
		if ($vars[0] === "fraise" && $vars[1] === "rouge" && $vars[2] === 2) {
			return "2 fraises rouges";
		}
		if ($vars[0] === "figue de barbarie" && $vars[1] === "pourpre" && $vars[2] === 2) {
			return "2 figues de barbarie pourpres";
		}
		if ($vars[0] === "ananas" && $vars[1] === "jaune" && $vars[2] === 3) {
			return "3 ananas jaunes";
		}
	}
}
# note : les ternaires sont présentes pour afficher la valeur de retour de la focntion si celle-ci est une boolean
print_r(((manger() === false) ? "FALSE" : "TRUE") . "\n"); // retourne FALSE
print_r(((manger("") === false) ? "FALSE" : "TRUE") . "\n"); // retourne FALSE
print_r(((manger(null) === false) ? "FALSE" : "TRUE") . "\n"); // retourne FALSE
print_r(manger("banane") . "\n"); // retourne "1 banane"
print_r(manger("banane", "jaune") . "\n"); // retourne "1 banane jaune"
print_r(((manger("banane", "bleue", null) === false) ? "FALSE" : "TRUE") . "\n"); // retourne FALSE
print_r(manger("banane", "mauve", 0) . "\n"); // retourne "0 banane mauve"
print_r(manger("banane", "jaune", 4) . "\n"); // retourne "4 bananes jaunes"
print_r(manger("abricot", "orange", 1) . "\n"); // retourne "1 abricot orange"
print_r(manger("fraise", "rouge", 2) . "\n"); // retourne "2 fraises rouges"
print_r(manger("figue de barbarie", "pourpre", 2) . "\n"); // retourne "2 figues de barbarie pourpres"
print_r(manger("ananas", "jaune", 3) . "\n"); // retourne "3 ananas jaunes"

?>
